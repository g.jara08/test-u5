package launcher;




import java.util.logging.Level;
import java.util.logging.Logger;

import model.Network;
import util.Wine;

public class App {

    public static void main(String[] args) {
        Logger logger = Logger.getGlobal();
        Wine wineTest = new Wine();
        Network<Integer>.Results results = wineTest.classify();
        logger.log(Level.INFO ,results.correct + " correct of " + results.trials + " = " +
                results.percentage * 100 + "%");
    }
}

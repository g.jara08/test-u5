// Neuron.java
// From Classic Computer Science Problems in Java Chapter 7
// Copyright 2020 David Kopec
//
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package model;

import util.Util;

import java.util.function.DoubleUnaryOperator;

public class Neuron {
	private double[] weights;
	private final double learningRate;
	private double outputCache;
	private double delta;
	private final DoubleUnaryOperator activationFunction;
	private final DoubleUnaryOperator derivativeActivationFunction;

	public Neuron(double[] weights, double learningRate, DoubleUnaryOperator activationFunction,
			DoubleUnaryOperator derivativeActivationFunction) {
		this.weights = weights;
		this.learningRate = learningRate;
		outputCache = 0.0;
		delta = 0.0;
		this.activationFunction = activationFunction;
		this.derivativeActivationFunction = derivativeActivationFunction;
	}

  public double[] getWeights() {
    return weights;
  }

  public void setWeights(double[] weights) {
    this.weights = weights;
  }

  public double getLearningRate() {
    return learningRate;
  }

  public double getOutputCache() {
    return outputCache;
  }

  public void setOutputCache(double outputCache) {
    this.outputCache = outputCache;
  }

  public double getDelta() {
    return delta;
  }

  public void setDelta(double delta) {
    this.delta = delta;
  }

  public DoubleUnaryOperator getActivationFunction() {
    return activationFunction;
  }

  public DoubleUnaryOperator getDerivativeActivationFunction() {
    return derivativeActivationFunction;
  }

	public double output(double[] inputs) {
		outputCache = Util.dotProduct(inputs, weights);
		return activationFunction.applyAsDouble(outputCache);
	}

}
